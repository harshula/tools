#!/bin/bash

# Copyright (C) 2023 Harshula Jayasuriya
# https://gitlab.com/harshula/tools/decompile-binaries.sh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


if [ $# -eq 0 ]
then
	echo "$0: need at least one argument"
	echo "USAGE: $0 BINARY-FILE [BINARY-FILE]*"
	exit 1
fi

call_ar () {
	file=$1

	dir="$(echo $file | sed -e 's/\.a//')"
	filepath="$(realpath $file)"
	mkdir $dir
	(cd $dir;
	 ar -x $filepath;
	 for objfile in $(ls *.o)
	 do
		objdump -D $objfile  > "${objfile}.asm"
	 done
        )
}

call_ldd () {
	file=$1
	if ! ldd $file > "$file.ldd"
	then
		rm "$file.ldd"
	fi
}

call_nm () {
	file=$1
	if ! nm $file > "$file.nm"
	then
		rm "$file.nm"
	fi
}

call_objdump () {
	file=$1
	if ! objdump -D $file > "$file.asm"
	then
		rm "$file.asm"
	fi
}

find $* -type f | \
while read -r file
do
	file_type="$(file -b --mime-type $file)"

	case $file_type in

	"application/x-executable"|"application/x-pie-executable")
		echo "executable: $file"
		call_ldd $file
		call_nm $file
		call_objdump $file
		;;
	"application/x-archive")
		echo "archive: $file"
		call_ldd $file
		call_nm $file
		call_ar $file
		;;
	"application/x-object")
		echo "object: $file"
		call_nm $file
		call_objdump $file
		;;
	*)
		continue
		;;
	esac
done
